package tyba.data;

import com.github.javafaker.Faker;
import tyba.model.request.JsonCredentials;

import java.security.SecureRandom;
import java.util.Locale;

public class CredentialsRequest {

    private CredentialsRequest(){
    }

    private static Faker faker = Faker.instance(new Locale("es", "CO"), new SecureRandom());

    public static JsonCredentials validCredentials(String username, String password){
        return JsonCredentials.builder()
                .username(username)
                .password(password)
                .build();
    }

    public static JsonCredentials wrongUsername(String password){
        return JsonCredentials.builder()
                .username(faker.internet().domainName())
                .password(password)
                .build();
    }

    public static JsonCredentials wrongPassword(String username){
        return JsonCredentials.builder()
                .username(username)
                .password(faker.internet().password())
                .build();
    }

    public static JsonCredentials wrongCredentials(){
        return JsonCredentials.builder()
                .username(faker.internet().domainName())
                .password(faker.internet().password())
                .build();
    }

    public static String wrongToken(){
        return faker.internet().password();
    }

}
