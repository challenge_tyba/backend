package steps.update_booking;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.data.BookingRequest;
import tyba.model.request.JsonBooking;
import tyba.model.response.JsonBookingResponse;
import tyba.tasks.CreateNewBooking;
import tyba.tasks.UpdateBooking;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

public class UpdateBookingSteps {

    Actor actor = Actor.named("Customer");
    EnvironmentVariables variables;
    int bookingId;

    @Before
    public void setup() {
        actor.whoCan(CallAnApi.at(variables.getProperty("base_url")));
    }

    @Given("a customer with booking create")
    public void a_customer_with_booking_create() {
        JsonBooking bookingData = BookingRequest.validReservation();
        actor.wasAbleTo(CreateNewBooking.withData(bookingData));
        JsonBookingResponse bookingResponse = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonBookingResponse.class);
        assertThat(bookingResponse).hasNoNullFieldsOrProperties();
        bookingId = bookingResponse.getBookingid();
    }


    @When("the customer tries to update the booking")
    public void the_customer_tries_to_update_the_booking() {
        JsonBooking bookingNewData = BookingRequest.validReservation();
        String authorization = variables.getProperty("authorization");
        actor.attemptsTo(UpdateBooking.withData(bookingId, bookingNewData, authorization));
    }

    @When("the customer tries to update the booking with wrong credentials")
    public void the_customer_tries_to_update_the_booking_with_wrong_credentials() {
        JsonBooking bookingNewData = BookingRequest.validReservation();
        String authorization = variables.getProperty("wrong_authorization");
        actor.attemptsTo(UpdateBooking.withData(bookingId, bookingNewData, authorization));
    }

    @Then("the customer should be update the booking successfully")
    public void the_customer_should_be_update_the_booking_successfully() {
        actor.should(seeThatResponse("Booking new information is return successfully",
                response -> response.statusCode(200)));
        JsonBooking bookingResponse = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonBooking.class);
        assertThat(bookingResponse).hasNoNullFieldsOrProperties();
    }

    @Then("the customer should see forbidden error")
    public void the_customer_should_see_forbidden_error() {
        actor.should(seeThatResponse("Forbidden error is return",
                response -> response.statusCode(403).body(containsString("Forbidden"))));
    }

}
