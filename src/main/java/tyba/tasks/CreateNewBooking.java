package tyba.tasks;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.model.request.JsonBooking;


public class CreateNewBooking implements Task {

    EnvironmentVariables variables;
    JsonBooking bookingData;

    public CreateNewBooking(JsonBooking bookingData) {
        this.bookingData = bookingData;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(variables.getProperty("path_booking"))
                .with(request -> request.contentType(ContentType.JSON)
                .body(bookingData)));
    }

    public static CreateNewBooking withData(JsonBooking bookingData) {
        return Tasks.instrumented(CreateNewBooking.class, bookingData);
    }

}
