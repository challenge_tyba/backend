#Author: Elkin Piedrahita

Feature: Update booking in booker api

  Scenario: Successful booking update
    Given a customer with booking create
    When the customer tries to update the booking
    Then the customer should be update the booking successfully

  Scenario: Failed booking update - invalid authentication
    Given a customer with booking create
    When the customer tries to update the booking with wrong credentials
    Then the customer should see forbidden error