package steps.authentication;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.data.CredentialsRequest;
import tyba.model.request.JsonCredentials;
import tyba.tasks.AuthenticateIntoBooking;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;

public class AuthenticationSteps {

    Actor actor = Actor.named("Customer");
    JsonCredentials credentials;
    EnvironmentVariables variables;
    String username;
    String password;

    @Before
    public void setup() {
        actor.whoCan(CallAnApi.at(variables.getProperty("base_url")));
        username = variables.getProperty("username");
        password = variables.getProperty("password");
    }

    @Given("a customer with valid username and password")
    public void a_customer_with_valid_username_and_password() {
        credentials = CredentialsRequest.validCredentials(username, password);
    }

    @Given("a customer with wrong username and valid password")
    public void a_customer_with_wrong_username_and_valid_password() {
        credentials = CredentialsRequest.wrongUsername(password);
    }

    @Given("a customer with valid username and wrong password")
    public void a_customer_with_valid_username_and_wrong_password() {
        credentials = CredentialsRequest.wrongPassword(username);
    }

    @Given("a customer with wrong username and password")
    public void a_customer_with_wrong_username_and_password() {
        credentials = CredentialsRequest.wrongCredentials();
    }

    @When("the customer tries to authenticate into booking platform")
    public void the_customer_tries_to_authenticate_into_booking_platform() {
        actor.attemptsTo(AuthenticateIntoBooking.platformWithCredentials(credentials));
    }

    @Then("the customer should authenticate successfully")
    public void the_customer_should_authenticate_successfully() {
        actor.should(seeThatResponse("Token is return successfully",
                response -> response.statusCode(200).body(startsWith("{\"token\":\""))));
    }

    @Then("the customer should see bad credentials error")
    public void the_customer_should_see_bad_credentials_error() {
        actor.should(seeThatResponse("Bad credentials error is return",
                response -> response.statusCode(200).body(containsString("{\"reason\":\"Bad credentials\""))));
    }
}
