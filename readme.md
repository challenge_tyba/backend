# Challenge automation backend Tyba 2022

Este proyecto Gradle - Java usando Serenity Rest y a través del patrón 
de automatización screenplay, implementa diferentes casos de prueba para
asegurar algunas API's del proyecto de reservas hoteleras disponible en
https://restful-booker.herokuapp.com/apidoc/index.html#api-Booking

Para ejecutar el proyecto por favor importar el build.gradle en el IDE Java de su
preferencia. Para ejecutar todos los casos de prueba por favor ejecutar el comando:


```
gradle clean test aggregate
```

## Los casos de prueba implementados son:

### Authentication in booker api:
- Successful authentication
- Failed authentication - wrong username
- Failed authentication - wrong password
- Failed authentication - wrong username and password

### Create booking in booker api:
- Successful booking creation
- Failed booking creation - incomplete data

### Get booking in booker api:
- Get an existing booking
- Get an not existing booking

###  Update booking in booker api:
- Successful booking update
- Failed booking update - invalid authentication

###  Delete booking in booker api:
- Successful booking delete
- Failed booking delete - invalid token
