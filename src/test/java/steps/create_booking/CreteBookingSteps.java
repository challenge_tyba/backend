package steps.create_booking;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.data.BookingRequest;
import tyba.model.request.JsonBooking;
import tyba.model.response.JsonBookingResponse;
import tyba.tasks.CreateNewBooking;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

public class CreteBookingSteps {

    Actor actor = Actor.named("Customer");
    JsonBooking bookingData;
    EnvironmentVariables variables;

    @Before
    public void setup() {
        actor.whoCan(CallAnApi.at(variables.getProperty("base_url")));
    }

    @Given("a customer with valid data for booking")
    public void a_customer_with_valid_data_for_booking() {
        bookingData = BookingRequest.validReservation();
    }

    @Given("a customer with incomplete data for booking")
    public void a_customer_with_incomplete_data_for_booking() {
        bookingData = BookingRequest.wrongReservation();
    }


    @When("the customer tries to create a new booking")
    public void the_customer_tries_to_create_a_new_booking() {
        actor.attemptsTo(CreateNewBooking.withData(bookingData));
    }

    @Then("the customer should be create a new booking successfully")
    public void the_customer_should_be_create_a_new_booking_successfully() {
        actor.should(seeThatResponse("Booking creation is successfully",
                response -> response.statusCode(200)));
        JsonBookingResponse bookingResponse = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonBookingResponse.class);
        assertThat(bookingResponse).hasNoNullFieldsOrProperties();
    }

    @Then("the customer should see generic error")
    public void the_customer_should_see_generic_error() {
        actor.should(seeThatResponse("Generic error is return",
                response -> response.statusCode(500).body(containsString("Internal Server Error"))));
    }

}
