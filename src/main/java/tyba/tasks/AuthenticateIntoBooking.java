package tyba.tasks;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.model.request.JsonCredentials;


public class AuthenticateIntoBooking implements Task {

    EnvironmentVariables variables;
    JsonCredentials credentials;

    public AuthenticateIntoBooking(JsonCredentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(variables.getProperty("path_auth"))
                .with(request -> request.contentType(ContentType.JSON)
                .body(credentials)));
    }

    public static AuthenticateIntoBooking platformWithCredentials(JsonCredentials credentials) {
        return Tasks.instrumented(AuthenticateIntoBooking.class, credentials);
    }

}
