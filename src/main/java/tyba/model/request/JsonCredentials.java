package tyba.model.request;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JsonCredentials {

    private String username;
    private String password;

}
