package tyba.model.response;


import lombok.Builder;
import lombok.Data;
import tyba.model.request.JsonBooking;

@Data
@Builder
public class JsonBookingResponse {

    private int bookingid;
    private JsonBooking booking;

}
