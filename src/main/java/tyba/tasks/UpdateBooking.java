package tyba.tasks;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Put;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.model.request.JsonBooking;


public class UpdateBooking implements Task {

    EnvironmentVariables variables;
    JsonBooking bookingData;
    int bookingId;
    String authorization;

    public UpdateBooking(int bookingId, JsonBooking bookingData, String authorization) {
        this.bookingId = bookingId;
        this.bookingData = bookingData;
        this.authorization = authorization;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        String path = variables.getProperty("path_booking") + bookingId;
        actor.attemptsTo(Put.to(path)
                .with(request -> request.contentType(ContentType.JSON)
                .header("Authorization", authorization)
                .body(bookingData)));
    }

    public static UpdateBooking withData(int bookingId, JsonBooking bookingData, String authorization) {
        return Tasks.instrumented(UpdateBooking.class, bookingId, bookingData, authorization);
    }

}
