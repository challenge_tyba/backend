package steps.get_booking;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.data.BookingRequest;
import tyba.model.request.JsonBooking;
import tyba.model.response.JsonBookingResponse;
import tyba.tasks.CreateNewBooking;
import tyba.tasks.GetBooking;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

public class GetBookingSteps {

    Actor actor = Actor.named("Customer");
    EnvironmentVariables variables;
    int bookingId;

    @Before
    public void setup() {
        actor.whoCan(CallAnApi.at(variables.getProperty("base_url")));
    }

    @Given("a customer with booking create")
    public void a_customer_with_booking_create() {
        JsonBooking bookingData = BookingRequest.validReservation();
        actor.wasAbleTo(CreateNewBooking.withData(bookingData));
        JsonBookingResponse bookingResponse = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonBookingResponse.class);
        assertThat(bookingResponse).hasNoNullFieldsOrProperties();
        bookingId = bookingResponse.getBookingid();
    }

    @Given("a not existing booking")
    public void a_not_existing_booking() {
        bookingId = BookingRequest.getNotExistingBookingId();
    }


    @When("the customer tries to get the booking information")
    public void the_customer_tries_to_get_the_booking_information() {
        actor.attemptsTo(GetBooking.withId(bookingId));
    }

    @Then("the customer should see the booking information")
    public void the_customer_should_see_the_booking_information() {
        actor.should(seeThatResponse("Booking information is return successfully",
                response -> response.statusCode(200)));
        JsonBooking bookingResponse = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonBooking.class);
        assertThat(bookingResponse).hasNoNullFieldsOrProperties();
    }

    @Then("the customer should see not found error")
    public void the_customer_should_see_not_found_error() {
        actor.should(seeThatResponse("Not found error is return",
                response -> response.statusCode(404).body(containsString("Not Found"))));
    }

}
