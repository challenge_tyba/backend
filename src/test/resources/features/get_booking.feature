#Author: Elkin Piedrahita

Feature: Get booking in booker api

  Scenario: Get an existing booking
    Given a customer with booking create
    When the customer tries to get the booking information
    Then the customer should see the booking information

  Scenario: Get an not existing booking
    Given a not existing booking
    When the customer tries to get the booking information
    Then the customer should see not found error