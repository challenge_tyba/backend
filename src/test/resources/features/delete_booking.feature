#Author: Elkin Piedrahita

Feature: Delete booking in booker api

  Scenario: Successful booking delete
    Given a customer with booking create
    When the customer tries to delete the booking
    Then the customer should be delete the booking successfully

  Scenario: Failed booking delete - invalid token
      Given a customer with booking create
      When the customer tries to delete the booking with wrong credentials
      Then the customer should see forbidden error