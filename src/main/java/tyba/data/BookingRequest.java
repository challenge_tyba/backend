package tyba.data;

import com.github.javafaker.Faker;
import tyba.model.request.JsonBooking;
import tyba.model.request.JsonBookingDates;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class BookingRequest {

    private BookingRequest(){
    }

    private static Faker faker = Faker.instance(new Locale("es", "CO"), new SecureRandom());

    public static JsonBooking validReservation(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime daysAfter = today.plusDays(10);
        Date minimumDate = Date.from(today.atZone(ZoneId.systemDefault()).toInstant());
        Date maximumDate = Date.from(daysAfter.atZone(ZoneId.systemDefault()).toInstant());
        Date initialDate = faker.date().between(minimumDate, maximumDate);
        Date finalDate = faker.date().between(initialDate, maximumDate);
        JsonBookingDates bookingDates = JsonBookingDates.builder()
                .checkin(sdf.format(initialDate))
                .checkout(sdf.format(finalDate))
                .build();
        return JsonBooking.builder()
                .firstname(faker.name().firstName())
                .lastname(faker.name().lastName())
                .totalprice(Integer.parseInt(faker.number().digits(6)))
                .depositpaid(faker.bool().bool())
                .bookingdates(bookingDates)
                .additionalneeds(faker.lorem().fixedString(20))
                .build();
    }

    public static JsonBooking wrongReservation(){
        return JsonBooking.builder()
                .lastname(faker.name().lastName())
                .totalprice(Integer.parseInt(faker.number().digits(6)))
                .depositpaid(faker.bool().bool())
                .additionalneeds(faker.lorem().fixedString(20))
                .build();
    }

    public static int getNotExistingBookingId(){
        return Integer.parseInt(faker.number().digits(7));
    }
}
