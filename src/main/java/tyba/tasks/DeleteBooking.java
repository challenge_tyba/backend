package tyba.tasks;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import net.thucydides.core.util.EnvironmentVariables;


public class DeleteBooking implements Task {

    EnvironmentVariables variables;
    int bookingId;
    String token;

    public DeleteBooking(int bookingId, String token) {
        this.bookingId = bookingId;
        this.token = token;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        String path = variables.getProperty("path_booking") + bookingId;
        actor.attemptsTo(Delete.from(path)
                .with(request -> request.contentType(ContentType.JSON)
                .header("Cookie", "token=" + token)));
    }

    public static DeleteBooking withId(int bookingId, String token) {
        return Tasks.instrumented(DeleteBooking.class, bookingId, token);
    }

}
