#Author: Elkin Piedrahita

Feature: Authentication in booker api

  Scenario: Successful authentication
    Given a customer with valid username and password
    When the customer tries to authenticate into booking platform
    Then the customer should authenticate successfully

  Scenario: Failed authentication - wrong username
    Given a customer with wrong username and valid password
    When the customer tries to authenticate into booking platform
    Then the customer should see bad credentials error

  Scenario: Failed authentication - wrong password
    Given a customer with valid username and wrong password
    When the customer tries to authenticate into booking platform
    Then the customer should see bad credentials error

  Scenario: Failed authentication - wrong username and password
    Given a customer with wrong username and password
    When the customer tries to authenticate into booking platform
    Then the customer should see bad credentials error