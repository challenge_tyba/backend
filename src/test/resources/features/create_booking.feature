#Author: Elkin Piedrahita

Feature: Create booking in booker api

  Scenario: Successful booking creation
    Given a customer with valid data for booking
    When the customer tries to create a new booking
    Then the customer should be create a new booking successfully

  Scenario: Failed booking creation - incomplete data
    Given a customer with incomplete data for booking
    When the customer tries to create a new booking
    Then the customer should see generic error