package steps.delete_booking;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import tyba.data.BookingRequest;
import tyba.data.CredentialsRequest;
import tyba.model.request.JsonBooking;
import tyba.model.request.JsonCredentials;
import tyba.model.response.JsonBookingResponse;
import tyba.tasks.AuthenticateIntoBooking;
import tyba.tasks.CreateNewBooking;
import tyba.tasks.DeleteBooking;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

public class DeleteBookingSteps {

    Actor actor = Actor.named("Customer");
    EnvironmentVariables variables;
    int bookingId;
    String token;

    @Before
    public void setup() {
        actor.whoCan(CallAnApi.at(variables.getProperty("base_url")));
    }

    @Given("a customer with booking create")
    public void a_customer_with_booking_create() {
        JsonBooking bookingData = BookingRequest.validReservation();
        actor.wasAbleTo(CreateNewBooking.withData(bookingData));
        JsonBookingResponse bookingResponse = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("", JsonBookingResponse.class);
        assertThat(bookingResponse).hasNoNullFieldsOrProperties();
        bookingId = bookingResponse.getBookingid();
    }


    @When("the customer tries to delete the booking")
    public void the_customer_tries_to_delete_the_booking() {
        String username = variables.getProperty("username");
        String password = variables.getProperty("password");
        JsonCredentials credentials = CredentialsRequest.validCredentials(username, password);
        actor.wasAbleTo(AuthenticateIntoBooking.platformWithCredentials(credentials));
        String token = SerenityRest.lastResponse()
                .jsonPath()
                .getObject("token", String.class);
        actor.attemptsTo(DeleteBooking.withId(bookingId, token));
    }

    @When("the customer tries to delete the booking with wrong credentials")
    public void the_customer_tries_to_delete_the_booking_with_wrong_credentials() {
        String token = CredentialsRequest.wrongToken();
        actor.attemptsTo(DeleteBooking.withId(bookingId, token));
    }

    @Then("the customer should be delete the booking successfully")
    public void the_customer_should_be_delete_the_booking_successfully() {
        actor.should(seeThatResponse("Booking delete is successfully",
                response -> response.statusCode(201)));
    }

    @Then("the customer should see forbidden error")
    public void the_customer_should_see_forbidden_error() {
        actor.should(seeThatResponse("Forbidden error is return",
                response -> response.statusCode(403).body(containsString("Forbidden"))));
    }

}
