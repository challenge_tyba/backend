package tyba.model.request;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JsonBooking {

    private String firstname;
    private String lastname;
    private int totalprice;
    private boolean depositpaid;
    private JsonBookingDates bookingdates;
    private String additionalneeds;
}
