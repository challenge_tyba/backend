package tyba.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.util.EnvironmentVariables;


public class GetBooking implements Task {

    EnvironmentVariables variables;
    int bookingId;

    public GetBooking(int bookingId) {
        this.bookingId = bookingId;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        String path = variables.getProperty("path_booking") + bookingId;
        actor.attemptsTo(Get.resource(path));
    }

    public static GetBooking withId(int bookingId) {
        return Tasks.instrumented(GetBooking.class, bookingId);
    }

}
